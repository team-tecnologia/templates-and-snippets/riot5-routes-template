import { resolveRoutes } from './helpers/routes'
import examplePolice from './policies/example'

/**
 * DON'T add / at the end
 * the path is used on riot/route, is defined based on 'path-to-regexp' module:
 * https://github.com/pillarjs/path-to-regexp
 *
 * The route system is based in events, so if a route match 2 defined routes
 * the app will mount both
 * */

const routes = {
    login: {
        path: '',
        component: 'pg-login',
        policies: [],
    },
    home: {
        path: '/home',
        component: 'pg-home',
        policies: [],
    },
    home2: {
        path: '/home/:id(\\d+)',
        component: 'pg-home',
        policies: [examplePolice],
    },
}

resolveRoutes(routes)

export default routes
