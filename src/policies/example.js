export default (userData) => userData.permissions.some((p) => (
    p.role === 'example1' || p.role === 'example2'
))
