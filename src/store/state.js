import { Url } from 'url'

const State = () => ({
    userData: {
        token: '',
        permissions: [
            { role: 'example1' },
            { role: 'example2' },
        ],
    },
    currRoute: {
        url: new Url(),
        found: false,
        name: '',
        data: {},
        allowed: false,
    },
    stateExample: {
        something: 'initial-state',
    },
})

export default State
