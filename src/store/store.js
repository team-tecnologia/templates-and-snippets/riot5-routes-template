// Meiosis State Management
// https://meiosis.js.org

import flyd from 'flyd'
import merge from 'mergerino'
import Actions from './actions'
import State from './state'

const setStates = () => {
    const update$ = flyd.stream()
    const states$ = flyd.scan(merge, State(), update$)
    const actions = Actions(update$, states$)
    return {
        states$,
        actions,
    }
}

const store = {
    ...setStates(),
    resetStates() {
        const { states$, actions } = setStates()
        this.states$ = states$
        this.actions = actions
    },
}

export default store
