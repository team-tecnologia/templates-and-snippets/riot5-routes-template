const Actions = (update$, states$) => ({
    setCurrRoute(obj) {
        const updatedObj = {
            ...states$().currRoute,
            ...obj,
        }
        update$({ currRoute: updatedObj })
    },
    setUserData(obj) {
        const updatedObj = {
            ...states$().userData,
            ...obj,
        }
        update$({ userData: updatedObj })
    },
    actionExample(obj) {
        const updatedObj = {
            ...states$().stateExample,
            ...obj,
        }
        update$({ stateExample: updatedObj })
    },
})

export default Actions
