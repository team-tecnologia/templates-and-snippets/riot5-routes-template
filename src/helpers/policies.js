export const validate = (policies, userData) => {
    // return true if all policies return true
    const ret = !policies.some((p) => !p(userData))
    return ret
}
