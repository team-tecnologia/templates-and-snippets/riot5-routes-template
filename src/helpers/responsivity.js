/*
  Devices sizes:

  Defined based on this search: https://gbksoft.com/blog/common-screen-sizes-for-responsive-web-design/
  this values is used to determinate if screen is ultra, desktop, hd-desktop,
  md-desktop, tablet, mobile
  NOTE: this is used on responsivity helpers too, so be carefully on change this
*/
const devicesBreakPoints = {
    mobile: 0, // min px to be a mobile
    tablet: 600, // min px to be a tablet
    'md-desktop': 1280, // min px to be a md-desktop (more used)
    'hd-desktop': 1400, // min px to be a hd-desktop
    'full-desktop': 1920, // min px to be a full-desktop
    'ultra-desktop': 2000, // min px to be a ultra-desktop
}

export const checkEnvByWidth = () => {
    const width = window.innerWidth
    const f = Object.entries(devicesBreakPoints).reverse().find(([_, v]) => width >= v)
    return f[0]
}

export const checkEnvByAgend = () => {
    const userAgent = window.navigator.userAgent.toLowerCase()

    if (/Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) {
        return 'mobile'
    } if (/(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent)) {
        return 'tablet'
    }
    return 'desktop'
}

export const getAspectRadio = () => {
    /**
     * Commons aspects:
     * 32:9, 21:9, 16:9, 16:10, 4:3
     * https://www.displayninja.com/what-is-aspect-ratio/
     */
    const gcd = (a, b) => ((b === 0) ? a : gcd(b, a % b))
    const w = window.screen.width
    const h = window.screen.height
    const r = gcd(w, h)
    let aw = w / r
    let ah = h / r

    if (aw === 7 || ah === 7) {
        aw *= 3
        ah *= 3
    } else if (aw === 5 || ah === 5) {
        aw *= 2
        ah *= 2
    }

    return `${aw}:${ah}`
}
