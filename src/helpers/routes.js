/**
 * Inject in each route a regexExp to handle with queryString
 * @param {<object>} routes {myroute:{ path: '', component: 'pg-home'}}
 */
export const resolveRoutes = (routes) => Object.entries(routes).map(([_, r]) => {
    if (r.path === '') return r

    const paths = r.path.split('/')
    const last = paths[paths.length - 1]
    const qExp = '([\\?][^?/]*)?'

    if (last.includes(':')) {
        if (last.includes('(')) {
            r.path += qExp
        } else {
            r.path += `([^?/]*)${qExp}`
        }
    } else {
        r.path += qExp
    }

    return r
})
