import routes from './routes'

const navLinks = [
    {
        type: 'group',
        text: 'Inicio grupo',
        items: [
            {
                type: 'item',
                text: 'Inicio com params',
                path: '/home/2?q[]=q1&q[]=q2',
                policies: routes.home2.policies,
            },
            {
                type: 'item',
                text: 'Inicio/2',
                path: '/home/2',
                policies: routes.home2.policies,
            },
        ],
    },
    {
        type: 'item',
        text: 'Inicio',
        path: '/home',
        policies: routes.home.policies,
    },
]

const toHashBased = (links) => {
    links.forEach((l) => {
        if (l.type === 'item') l.path = `#${l.path}`
        else toHashBased(l.items)
    })
}

toHashBased(navLinks)

export {
    navLinks,
}
