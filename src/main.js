import { component, install } from 'riot'
import './media/favicon.png'

import store from './store/store'

import App from './app.riot'

install((c) => {
    c.states$ = store.states$
    c.actions = store.actions
    return c
})

/* global DEVELOPMENT, STAGE */
if (DEVELOPMENT || STAGE) {
    window.state = store.states$
}

component(App)(document.getElementById('root'))
