# riot5+meiosis+new-route+policies
This stack is a little different from riot5+meiosis, because the route system is a little different
and this have a policies stack

## '/' Based routes
To use with '/' based routes, your sever must return for all routes the same resources.

E.g.: the route my_site.com and my_site.com/anything must return from server the index.html page,
and than, the index.html will get the bundled js and handle with routes.

For that use, may be necessary add to webpack.configs.js inside 'output' key a ```publicPath: '/'```.

And add '/' at the beginning for all hardcoded resources, like at 'index.html' or 'home.riot' where we use '/media/favicon.png'.

Using all resources with absolute path '/', we can use normally the folders 'font', 'media' ...,
without the server return the index.html page.

The dev server used is already configured to this stuff.

## '#' Based routes
To use with '#' based routes, just keep the project like are current.

This method is required to use with gitlab pages, because gitlab's server don't return 'index.html'
for all paths.


## Files structure
.
├── src                      # Folder with all code base
│   ├── app.riot                # Entry point for the app. Here you must import your pages components
│   ├── app.scss                # Global scss file, that import some scss module from style folder
│   ├── components              # All app components, with they on scss file if needed (e.g.: nav.riot nav.scss), can have sub folders, to separate context
│   ├── fonts                   # All fonts and icons packages file, like .svg, .otf, ...
│   ├── helpers                 # Javascript modules that are useful. Used to decrease component code too
│   ├── links.js                # Module with all app static links
│   ├── main.js                 # Script that instance the app, the app store, and link then together
│   ├── media                   # All assets files, like: video, imgs, svg...
│   ├── pages                   # All riot pages components with their on scss files
│   ├── policies                # Javascript module that define some access policy
│   ├── routes.js               # Module with app routers and theirs policies
│   ├── store                   # Folder with app global state
│   │   ├── actions.js          # Action to change the state
│   │   ├── state.js            # Initial state
│   │   └── store.js            # Module to implement the app state
│   └── style                   # Folder with global styles,  splitted into different files
└── test                     # Folder with all test: unitary, integration ....
    ├── components-factory      # Folder with riot components, used to test other components, to test thing like slots
    ├── helpers                 # JS files to helper with something
    └── mock-data               # JS files to mock riot props, app state, and others

## Code patterns
This project is configured with lint, and editor config.

> If you have some improve for this, consider do the change at repo > snippets > riot5+meiosis+new-route+policies too. Him is our template project

The same code pattern applied to normal js files, must be used to .riot files for script parts.
The lint can't do nothing with .riot files only .js.

The global scss must be at app.scss or imported by him. The folder style keeps the global scss, and
some common modules that can be imported by others scss files.

### Git
git commits messages must follow the pattern <scope> (<type>) : <subject>. The link bellow show details.

> Note that the convention at the link follow an different order.

> https://dev.to/i5han3/git-commit-message-convention-that-you-can-follow-1709

### File names
All files name, must be in lower case, separated by dash -.

Page must follow the name pg-<page-name>.riot, with the respective scss: file pg-<page-name>.scss.

#### Mocks file names
Mock files for **components** must have the same name of the component with prefix 'mock-', suffix '-props', and have to be inside a folder with the same context name like:
- mock-<pg-file-name>-props.js -> inside the folder mock-data > pages
- mock-<my-component>-props.js -> inside the folder mock-data > components
- mock-<my-component-name>-props.js -> inside the folder mock-data > components > my-sub-components-folder

Mock files for **js files** must have the same name too, with the prefix 'mock-', suffix '-params', and inside the folder context like:
- mock-<my-helper-file-name>-params.js -> inside the folder mock-data > helpers
- mock-<my-policy-file-name>-params.js -> inside the folder mock-data > policies

### Html ids and classes
Keep the ids and classes in lower case, separated by dash -. Following this pattern:
http://getbem.com/introduction/

### Version
if versioning production tags, follow this:
https://semver.org/lang/pt-BR/

## Getting started

Clone the template

```sh
git clone --depth 1 https://gitlab.com/team-tecnologia/templates-and-snippets/riot5-routes-template project-name
```

Remove old git repo

```sh
cd project-name
rm -rf .git
```

Install plugins

```sh
npm install
```

Run the app

```sh
npm run start
```

Build for production

```sh
npm run build
```
